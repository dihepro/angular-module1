import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CanchaListaComponent } from './cancha-lista.component';

describe('CanchaListaComponent', () => {
  let component: CanchaListaComponent;
  let fixture: ComponentFixture<CanchaListaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CanchaListaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CanchaListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
