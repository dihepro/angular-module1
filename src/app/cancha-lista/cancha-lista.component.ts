import { Component, OnInit } from '@angular/core';
import { Cancha } from './../models/Cancha.models';

@Component({
  selector: 'app-cancha-lista',
  templateUrl: './cancha-lista.component.html',
  styleUrls: ['./cancha-lista.component.css']
})
export class CanchaListaComponent implements OnInit {

  canchasLista: Cancha[];

  constructor() { 
    this.canchasLista = [];
  }

  ngOnInit(): void {
  }

  guardar(i: string, n: string): boolean{
    this.canchasLista.push(new Cancha(i, n))
    return false;
  }

}
