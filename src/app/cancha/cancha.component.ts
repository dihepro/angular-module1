import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { Cancha } from './../models/Cancha.models';

@Component({
  selector: 'app-cancha',
  templateUrl: './cancha.component.html',
  styleUrls: ['./cancha.component.css']
})
export class CanchaComponent implements OnInit {
  @Input() cancha: Cancha;
  @HostBinding('attr.class') ccsClass = 'col-md-4';
  constructor() { }

  ngOnInit(): void {
  }

}
