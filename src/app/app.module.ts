import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CanchaComponent } from './cancha/cancha.component';
import { CanchaListaComponent } from './cancha-lista/cancha-lista.component';

@NgModule({
  declarations: [
    AppComponent,
    CanchaComponent,
    CanchaListaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
